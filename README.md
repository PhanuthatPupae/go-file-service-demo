# File service demo (Backend)
## Auth with JWT and Mock Email Service By Postman mock service
### Separated Commamds for Run Development Server Locally


1. Start PostgreSQL docker

```sh
./tools/start_postgresql_docker.sh
```

2. Start MinIO docker

```sh
./tools/start_minio_docker.sh
```

3. Reset Database. Your previous data will be lost.

```sh
./tools/reset_db.sh
```

3. Migrate Table.

```sh
./tools/migrate.sh
```

5. Start HTTP Server using `config.yaml` as a configuration.
```sh
./tools/serve.sh
```

#### Province PostMan Collecttion
file_name: docs/file_service_go_basic.postman_collection.json

#### Seq Diagram
![plot](./docs/overall.png)