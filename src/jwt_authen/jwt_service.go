package jwt_authen

import (
	"context"

	"go.uber.org/zap"

	"file-service-go/logger"
)

type jwtService struct {
	log    *zap.SugaredLogger
	config *Config
}

func New(context context.Context, requestId string) (jwtService, error) {
	log := logger.Logger.With(
		"request_id", requestId,
		"part", "jwt_service",
	)
	config, err := InitConfig()
	if err != nil {
		return jwtService{}, err
	}
	return jwtService{
		log:    log,
		config: config,
	}, nil
}
