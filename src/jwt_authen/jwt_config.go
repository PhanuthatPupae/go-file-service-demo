package jwt_authen

import (
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	JWTPublicKeyPath  string
	JWTPrivateKeyPath string

	// token properties
	ExpiresIn           time.Duration
	SetPasswordTokenTTL time.Duration
	TokenIssuer         string
}

func InitConfig() (*Config, error) {
	return &Config{
		JWTPublicKeyPath:    viper.GetString("TokenSignerPublicKeyPath"),
		JWTPrivateKeyPath:   viper.GetString("TokenSignerPrivateKeyPath"),
		ExpiresIn:           viper.GetDuration("TokenExpiresIn") * time.Second,
		SetPasswordTokenTTL: viper.GetDuration("SetPasswordTokenExpiresIn") * time.Second,
		TokenIssuer:         viper.GetString("TokenIssuer"),
	}, nil
}
