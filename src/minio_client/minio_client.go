package minioclient

import (
	"bytes"
	"context"
	"fmt"
	"net/url"
	"strings"
	"time"

	"github.com/gabriel-vasile/mimetype"
	"github.com/minio/minio-go/v7"
)

func (mc *minioClientService) CreateBucket() error {
	err := mc.Minio.MakeBucket(
		context.Background(),
		mc.config.BucketName,
		minio.MakeBucketOptions{Region: "th"},
	)

	if err != nil {
		exists, errBucketExists := mc.Minio.BucketExists(context.Background(), mc.config.BucketName)
		if errBucketExists == nil && exists {
			mc.log.Infof("Bucket: %s already exists", mc.config.BucketName)
			return nil
		}

		return err
	}

	mc.log.Infof("Successfully created %s", mc.config.BucketName)

	return nil
}

func (mc *minioClientService) UploadFileToBucket(
	objectName string,
	fileContent []byte,
) (key string, err error) {
	reader := bytes.NewReader(fileContent)

	contentType := mimetype.Detect(fileContent)
	info, err := mc.Minio.PutObject(
		context.Background(),
		mc.config.BucketName,
		objectName,
		reader,
		reader.Size(),
		minio.PutObjectOptions{ContentType: contentType.String()},
	)
	if err != nil {
		return "", err
	}
	return info.Key, nil
}

func (mc *minioClientService) GetFileFromBucket(
	objectName string,
) ([]byte, error) {

	object, err := mc.Minio.GetObject(
		context.Background(),
		mc.config.BucketName,
		objectName,
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, err
	}

	buf := &bytes.Buffer{}
	buf.ReadFrom(object)

	return buf.Bytes(), nil
}

func (mc *minioClientService) DeleteFileFromBucket(
	objectName string,
) error {

	err := mc.Minio.RemoveObject(
		context.Background(),
		mc.config.BucketName,
		objectName,
		minio.RemoveObjectOptions{},
	)
	if err != nil {
		return err
	}

	return nil
}

func (mc *minioClientService) GetUrlFromPresigned(
	objectName string,
	filename string,
	fileType string,
) (string, error) {
	reqParams := make(url.Values)
	reqParams.Set("response-content-disposition", "attachment; filename=\""+filename+"."+fileType+"\"")

	presignedURL, err := mc.Minio.PresignedGetObject(
		context.Background(),
		mc.config.BucketName,
		objectName,
		time.Hour*24*3,
		reqParams,
	)
	if err != nil {
		return "", err
	}

	if mc.config.EndpointUrl != "" {
		endpointUrl, err := url.Parse(mc.config.EndpointUrl)
		if err != nil {
			return "", err
		}
		presignedURL.Scheme = endpointUrl.Scheme
		presignedURL.Host = endpointUrl.Host
	}

	return strings.Replace(presignedURL.String(), "&", "%5Cu0026", 1), nil
}

func (mc *minioClientService) GetNoExpireObjectURL(
	objectName string,
	originalNameWithExtension string,
) (
	string, error,
) {

	reqParams := make(url.Values)
	reqParams.Set("response-content-disposition", `attachment; filename=\"`+originalNameWithExtension+`\"`)

	presignedURL, err := mc.Minio.PresignedGetObject(
		context.Background(),
		mc.config.BucketName,
		objectName,
		time.Hour*24*3,
		reqParams,
	)
	if err != nil {
		return "", err
	}

	if mc.config.EndpointUrl != "" {
		endpointUrl, err := url.Parse(mc.config.EndpointUrl)
		if err != nil {
			return "", err
		}
		presignedURL.Scheme = endpointUrl.Scheme
		presignedURL.Host = endpointUrl.Host
	}

	return fmt.Sprintf("%s://%s/%s/%s",
		presignedURL.Scheme,
		presignedURL.Host,
		mc.config.BucketName,
		objectName,
	), nil
}
