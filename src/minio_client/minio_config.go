package minioclient

import "github.com/spf13/viper"

type Config struct {
	Endpoint        string
	EndpointUrl     string
	AccessKeyID     string
	SecretAccessKey string
	UseSSL          bool
	BucketName      string
}

func InitConfig() (*Config, error) {
	config := &Config{
		Endpoint:        viper.GetString("ObjectStorage.Endpoint"),
		EndpointUrl:     "",
		AccessKeyID:     viper.GetString("ObjectStorage.AccessKeyID"),
		SecretAccessKey: viper.GetString("ObjectStorage.SecretAccessKey"),
		UseSSL:          viper.GetBool("ObjectStorage.UseSSL"),
		BucketName:      viper.GetString("ObjectStorage.BucketName"),
	}

	if config.Endpoint == "" {
		config.Endpoint = "localhost"
	}

	return config, nil
}

