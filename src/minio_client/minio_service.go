package minioclient

import (
	"context"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"go.uber.org/zap"

	"file-service-go/logger"
)

type minioClientService struct {
	log    *zap.SugaredLogger
	config *Config
	Minio  *minio.Client
}

func New(context context.Context, requestId string) (*minioClientService, error) {

	config, err := InitConfig()
	if err != nil {
		return nil, err
	}

	log := logger.Logger.With(
		"request_id", requestId,
		"part", "minio",
	)

	minioClient, err := minio.New(config.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(config.AccessKeyID, config.SecretAccessKey, ""),
		Secure: config.UseSSL,
	})

	if err != nil {
		return nil, err
	}

	service := &minioClientService{
		log:    log,
		config: config,
		Minio:  minioClient,
	}

	err = minioClient.MakeBucket(
		context,
		config.BucketName,
		minio.MakeBucketOptions{Region: "th"},
	)

	if err != nil {
		exists, errBucketExists := minioClient.BucketExists(context, config.BucketName)
		if errBucketExists == nil && exists {
			log.Infof("Bucket: %s already exists", config.BucketName)
			return service, nil
		}

		return nil, err
	}

	log.Infof("Successfully created %s", config.BucketName)

	return service, nil
}

func (objStorage *minioClientService) Close() error {
	return nil
}
