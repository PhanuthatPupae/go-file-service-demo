package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"file-service-go/custom_error"
	"file-service-go/http/response"
	"file-service-go/service"
)

func Auth(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to Auth")
	responseOutput := response.ResponseOutput{}

	input := service.AuthInput{}

	if err := context.ShouldBindJSON(&input); err != nil {
		apiLogger.Errorf("bind json body to upload file error: %s", err.Error())
		responseOutput.Code = custom_error.InvalidJSONString
		responseOutput.Message = err.Error()
		context.JSON(http.StatusBadRequest, responseOutput)
		return
	}

	requestId, _ := context.Get("request_id")
	requestIdStr := requestId.(string)

	service := service.New(requestIdStr)
	output, err := service.Auth(input)
	if err != nil {
		context.JSON(http.StatusInternalServerError, responseOutput)
		return
	}

	if output.Token != "" {
		responseOutput.Message = "success"
		responseOutput.Data = output.Token
		context.JSON(http.StatusOK, responseOutput)
		return
	} else {
		responseOutput.Message = "unauthorized"
		context.JSON(http.StatusOK, responseOutput)
		return
	}

}
