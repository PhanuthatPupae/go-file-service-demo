package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"file-service-go/custom_error"
	"file-service-go/http/response"
	"file-service-go/service"
)

func UploadFile(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to UploadFile")
	responseOutput := response.ResponseOutput{}

	requestId, _ := context.Get("request_id")
	requestIdStr := requestId.(string)

	input := service.UploadFileInput{}
	err := context.Request.ParseMultipartForm(10 << 20)
	if err != nil {
		apiLogger.Errorf("ParseMultipartForm error: %s", err.Error())
		responseOutput.Code = custom_error.InvalidMultiplePath
		responseOutput.Message = err.Error()
		context.JSON(http.StatusBadRequest, responseOutput)
		return
	}

	f := context.Request.MultipartForm

	input.File = f

	fileService := service.New(requestIdStr)
	output, err := fileService.UploadFile(input)
	if err != nil {
		context.JSON(http.StatusInternalServerError, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = output.FileName
	context.JSON(http.StatusOK, responseOutput)
}

//TODO get file
