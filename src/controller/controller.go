package controller

import (
	"go.uber.org/zap"

	"file-service-go/logger"
)

type Controller struct {
	RequestId string
	Logger    *zap.SugaredLogger
}

func New(requestId *string, userId *string) Controller {

	controllerObj := Controller{
		RequestId: *requestId,
	}

	controllerObj.Logger = logger.Logger.With(
		"request_id", *requestId,
		"part", "controller",
	)

	return controllerObj
}
