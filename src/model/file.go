package model

import (
	"time"

	"gorm.io/gorm"
)

type File struct {
	FileName    string
	FileType    string
	ContentType string
	ID          uint           `gorm:"primarykey" json:"id"`
	CreatedAt   time.Time      `json:"created_at" gorm:"type:timestamptz"`
	UpdatedAt   time.Time      `json:"updated_at" gorm:"type:timestamptz;autoUpdateTime"`
	DeletedAt   gorm.DeletedAt `gorm:"index" json:"deleted_at,omitempty"`
}
