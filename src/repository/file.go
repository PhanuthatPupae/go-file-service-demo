package repository

import (
	"file-service-go/database/postgresql"
	"file-service-go/model"
)

func (repo Repository) CreateFile(file model.File) (model.File, error) {
	err := postgresql.GormDB.Model(&model.File{}).Create(file).Error
	if err != nil {
		return file, err
	}
	return file, nil
}
