package repository

import (
	"go.uber.org/zap"

	"file-service-go/logger"
)

type Repository struct {
	RequestId string
	Logger    *zap.SugaredLogger
}

func New(requestId string) Repository {
	modelObj := Repository{
		RequestId: requestId,
	}
	modelObj.Logger = logger.Logger.With(
		"request_id", requestId,
		"part", "repository",
	)
	return modelObj
}
