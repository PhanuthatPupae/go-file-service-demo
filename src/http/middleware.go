package http

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.uber.org/zap"

	"file-service-go/jwt_authen"
	"file-service-go/logger"
	"file-service-go/utils"
)

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()
		path := c.Request.URL.Path
		query := c.Request.URL.RawQuery

		c.Next()

		// Log Request
		latency := time.Since(t)
		requestId := c.GetString("request_id")
		logger.Logger.Infow(path,
			zap.String("request-id", requestId),
			zap.Int("status", c.Writer.Status()),
			zap.String("method", c.Request.Method),
			zap.String("path", path),
			zap.String("query", query),
			zap.String("ip", c.ClientIP()),
			zap.String("user-agent", c.Request.UserAgent()),
			zap.String("errors", c.Errors.ByType(gin.ErrorTypePrivate).String()),
			zap.Duration("latency", latency),
		)
	}
}

func SetRequestId() gin.HandlerFunc {
	return func(c *gin.Context) {
		if _, exist := c.Get("request_id"); !exist {
			c.Set("request_id", uuid.New().String())
		}

		requestId, _ := c.Get("request_id")
		c.Set("logger", logger.Logger.With(
			"request_id", requestId,
			"part", "interface",
		))

		c.Next()
	}
}

func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {
		inputToken := c.Request.Header.Get("Authorization")
		token := utils.GetTokenStringFromBearerPrefix(inputToken)
		jwtService, err := jwt_authen.New(c, "")
		if err != nil {
			c.JSON(500, err.Error())
			c.Abort()
			return
		}
		_, err = jwtService.VerifyJWToken(c, token)
		if err != nil {
			c.JSON(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}
		c.Next()
	}
}
