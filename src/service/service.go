package service

import (
	"go.uber.org/zap"

	"file-service-go/logger"
)

type Service struct {
	RequestId string
	Logger    *zap.SugaredLogger
}

func New(requestId string) Service {
	serviceObj := Service{
		RequestId: requestId,
	}

	serviceObj.Logger = logger.Logger.With(
		"request_id", requestId,
		"part", "service",
	)

	return serviceObj
}
