package service

import (
	"context"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"path/filepath"
	"time"

	"file-service-go/mail"
	minioclient "file-service-go/minio_client"
	"file-service-go/model"
	"file-service-go/repository"
)

type UploadFileInput struct {
	File     *multipart.Form `json:"file"`
	FileName string          `json:"file_name"`
}

type UploadFileOutput struct {
	FileName []string `json:"file_name"`
}

func (service Service) UploadFile(input UploadFileInput) (UploadFileOutput, error) {
	service.Logger.Infof("start UploadFile")
	output := UploadFileOutput{}
	multipartFile := input.File.File

	for _, fileHeaders := range multipartFile {
		for _, fileHeader := range fileHeaders {
			file, err := fileHeader.Open()
			if err != nil {
				service.Logger.Errorf("cannot open file: %v", err)
				return output, err
			}
			defer file.Close()

			fileContent, err := ioutil.ReadAll(file)
			if err != nil {
				service.Logger.Errorf("cannot read file content: %v", err)
				return output, err
			}

			_, err = file.Seek(0, 0)
			if err != nil {
				service.Logger.Errorf("cannot rewind file pointer: %v", err)
				return output, err
			}

			objectName := input.FileName
			if objectName == "" {
				objectName = fmt.Sprintf("%d%s", time.Now().Unix(), fileHeader.Filename)
			}

			contentType := fileHeader.Header["Content-Type"][0]

			minioClient, err := minioclient.New(context.Background(), service.RequestId)
			if err != nil {
				service.Logger.Error("minioclient error:%s", err.Error())
				return output, err
			}

			key, err := minioClient.UploadFileToBucket(objectName, fileContent)
			if err != nil {
				service.Logger.Error("UploadFileToBucket error:%s", err.Error())
				return output, err
			}
			output.FileName = append(output.FileName, key)

			repo := repository.New(service.RequestId)
			_, err = repo.CreateFile(model.File{
				FileName:    objectName,
				FileType:    filepath.Ext(fileHeader.Filename),
				ContentType: contentType,
			})
			if err != nil {
				service.Logger.Error("CreateFile error:%s", err.Error())
				return output, err
			}

			mailService := mail.New(context.Background(), service.RequestId)
			_, err = mailService.SendEmail(mail.SendMailInput{
				SendTo: "test@gmail.com",
			})
			if err != nil {
				return output, err
			}
		}
	}

	service.Logger.Infof("UploadFile Done")
	return output, nil
}
