package service

import (
	"context"

	"file-service-go/jwt_authen"
)

type AuthInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type AuthOutput struct {
	Token string `json:"token"`
}

func (service Service) Auth(input AuthInput) (AuthOutput, error) {
	service.Logger.Infof("start Auth")
	output := AuthOutput{}
	jwtService, err := jwt_authen.New(context.Background(), service.RequestId)
	if err != nil {
		return output, err
	}

	//MOCK USER PASSWORD
	if input.Username == "test_99" && input.Password == "123456789" {
		token, err := jwtService.GenerateToken(context.Background(), jwt_authen.UserClaimsPayload{
			UserId: "for_test_1",
			UserNo: "test_99",
		})
		if err != nil {
			return output, err
		}
		output.Token = token
	}

	return output, nil
}
