package migration

import (
	"gorm.io/gorm"

	"file-service-go/model"
)

var CreateFileTable = &Migration{
	Number: 1,
	Name:   "create file table",
	Forwards: func(db *gorm.DB) error {
		err := db.AutoMigrate(&model.File{})
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateFileTable)
}
