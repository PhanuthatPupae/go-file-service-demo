package cmd

import (
	"github.com/spf13/cobra"

	migration "file-service-go/database/postgresql/migrate"
	"file-service-go/logger"
)

var MigrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "migrate",
	Run: func(cmd *cobra.Command, args []string) {
		// Init Logger
		logger.InitLogger()

		migration.Migrate(-1)
		// postgresql.InitGormDB(false)
		logger.SyncLogger()
	},
}

func init() {
	rootCmd.AddCommand(MigrateCmd)
}
