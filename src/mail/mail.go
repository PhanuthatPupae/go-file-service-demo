package mail

import (
	"context"

	"github.com/spf13/viper"
	"go.uber.org/zap"

	"file-service-go/logger"
)

type Mail struct {
	Context   context.Context
	RequestId string
	Logger    *zap.SugaredLogger
	UrlEmail  string
}

func New(context context.Context, requestId string) Mail {

	mail := Mail{
		RequestId: requestId,
	}

	mail.Logger = logger.Logger.With(
		"request_id", requestId,
		"part", "mail",
	)

	mail.Context = context
	mail.UrlEmail = viper.GetString("Mail.Url.SendMail")
	return mail
}
