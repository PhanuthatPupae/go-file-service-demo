#!/bin/sh


docker run -d --rm \
  -p 9000:9000 \
  -p 9003:9003 \
  -e "MINIO_ROOT_USER=minio" \
  -e "MINIO_ROOT_PASSWORD=hellotest" \
  --name file-service \
  minio/minio:RELEASE.2022-01-08T03-11-54Z server /data --console-address ":9003"

